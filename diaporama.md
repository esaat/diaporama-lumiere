# La Lumière

## Sources de lumière

Source primaire <!-- .element: class="fragment fade-up" -->
 : Elle produit sa propre lumière. Elle peut être naturelle ou artificielle.  <!-- .element: class="fragment fade-up" -->
   ex : Le Soleil, une ampoule, une bougie, une étoile, ...<!-- .element: class="fragment fade-up" -->

Source secondaire <!-- .element: class="fragment fade-up" -->
 : Elle diffuse la lumière provenant d'une autre source.  <!-- .element: class="fragment fade-up" -->
   ex : La Lune, un écran de cinéma, un chat, tout objet visible, ...<!-- .element: class="fragment fade-up" -->

---

## Propagation de la lumière

 La lumière se propage de façon rectiligne dans un milieu *transparent* et *homogène*.

---

 Expériences avec un LASER.

---

 Modèle du rayon lumineux.

---

## Vitesse de la lumière dans le vide

La vitesse de propagation de la lumière est appelée célérité. C'est une valeur très élevée dans le vide ou l'air.

. . .


![Célérité de la lumière dans le vide](images/celeriteExacte.png)

. . .

![Valeur approchée à retenir pour l'air ou le vide](images/celeriteApprox.png)

---

## Vitesse de la lumière dans un milieu transparent

Dans un milieu matériel transparent, la lumière se propage à une vitesse *plus faible* que dans le vide.

. . .

Par exemple, la vitesse de propagation de la lumière dans l'eau est :

. . .

$$c = 2,26 \times 10^{8} m \cdot s^{-1}$$
![Célérité de la lumière dans l'eau](images/celeriteEau.png)

---

## Indice optique d'un milieu matériel

On définit l'indice optique **n** d'un milieu matériel par le rapport des célérités de la lumière dans le vide et dans le milieu étudié.

. . .


![Indice optique](images/indiceOptique.png)

---

Par exemple l'indice optique de l'eau est :

. . .

![Indice de l'eau](images/indiceEau.png)

# Optique géométrique

## Réflexion de la lumière

. . .

### Plan d'incidence

. . .

Définition du rayon incident, point d'incidence **I**, normale au miroir et rayon réfléchi.

. . .

Plan d'incidence.

. . .

Angle d'incidence *i* et Angle de réflexion *r*

. . .

### Lois de Descartes pour la Réflexion

::: incremental

* Le rayon réfléchi se trouve dans le plan d'incidence.
* L'angle de réflexion est égal à l'angle d'incidence.

:::

. . .

i=r



## Réfraction de la lumière
